import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:encrypt/encrypt.dart' as Enc;
import 'dart:async';
import 'package:flutter/services.dart' show rootBundle;
import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'page2.dart';

String generateKey(String input) {
  return md5.convert(utf8.encode(input)).toString();
}

class Page1 extends StatefulWidget {
  Page1({Key? key}) : super(key: key);
  @override
  _Page1State createState() => _Page1State();
}

class _Page1State extends State<Page1> {
  final passField = TextEditingController();

  @override
  void dispose() {
    passField.dispose();
    super.dispose();
  }

  Future decrypt(String keyStr) async {
    final cipherText = await read();
    if (cipherText != 'None') {
      keyStr = generateKey(keyStr);

      print("Ключ: $keyStr");
      final key = Enc.Key.fromUtf8(keyStr);
      final iv = Enc.IV.fromLength(16);

      final encrypter = Enc.Encrypter(Enc.AES(key, mode: Enc.AESMode.cbc));
      final encryptedText = Enc.Encrypted.fromBase64(cipherText);
      try {
        List decrypted =
            json.decode(encrypter.decrypt(encryptedText, iv: iv).toString());
        print("Расшифрованное: $decrypted");
        return decrypted;
      } catch (error) {
        return "Error";
      }
    } else {
      print("Couldn't read file");
      return "Error";
    }
  }

  Future<String> read() async {
    try {
      print(rootBundle.loadString('assets/text.txt'));
      return await rootBundle.loadString('assets/text.txt');
    } catch (e) {
      return 'None';
    }
  }

  @override
  Widget build(BuildContext context) {
    // final test = [{"URL": "https://vk.com/", "Login": "Username1", "Password": "Password1"},{"URL": "https://about.gitlab.com/", "Login": "Username2", "Password": "Password2"},{"URL": "https://github.com/", "Login": "Username3", "Password": "Password3"}];
    // print(test[1]["URL"]);
    return Scaffold(
      backgroundColor: Colors.grey[50],
      appBar: AppBar(
        title: Text("Войдите"),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(12),
                  child: Container(
                    height: 60,
                    child: TextField(
                        controller: passField,
                        decoration: InputDecoration(
                          labelText: 'Введите пароль',
                        )),
                  ),
                ),
                TextButton(
                  onPressed: () async {
                    final dec = await decrypt(passField.text);
                    if (dec != "Error") {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Page2(content: dec)));
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text("Неверный пароль")));
                    }
                  },
                  child: Text(
                    'Вход',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w400),
                  ),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.blue),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
