import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:url_launcher/url_launcher.dart';

class Page2 extends StatefulWidget {
  final List content;
  Page2({Key? key, required this.content}) : super(key: key);
  @override
  _Page2State createState() => _Page2State();
}

class _Page2State extends State<Page2> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(widget.content.length);
    return Scaffold(
      backgroundColor: Colors.grey[50],
      appBar: AppBar(
        title: Text("Пароли"),
      ),
      body: Builder(
        builder: (context) => Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: ListView.separated(
                    separatorBuilder: (context, index) =>
                        Divider(color: Colors.grey[50]),
                    padding: EdgeInsets.all(8.0),
                    itemCount: widget.content.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        color: Colors.grey[50],
                        child: Center(
                            child: Container(
                          child: Row(
                            children: [
                              Container(
                                width: 100,
                                height: 100,
                                child: Image.network(
                                    "${widget.content[index]["URL"]}favicon.ico"),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Linkify(
                                      onOpen: _onOpen,
                                      text: widget.content[index]["URL"],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                        "Логин: ${widget.content[index]["Login"]}"),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                        "Пароль: ${widget.content[index]["Password"]}"),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )),
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _onOpen(LinkableElement link) async {
    if (await canLaunch(link.url)) {
      await launch(link.url);
    } else {
      throw 'Could not launch $link';
    }
  }
}
